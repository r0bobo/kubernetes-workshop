# Kubernetes base setup

K8s applications needed for a base functioning cluster.

## Setup for specific environment

For home:

```yaml
kubectl apply -k overlays/home
```

For Layer10 office:

```yaml
kubectl apply -k overlays/home
```

Might need to remove iscsi storageclass before applying settings.

```yaml
kubectl delete storageclass block
```

To customize for a new environment,
create a new overlay and update `patches.yaml` and `config` with relevant settings.
