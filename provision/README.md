# Instructions

## Installation
1. Made for Raspbian
1. Don't install docker
1. Add `cgroup_memory=3 cgroup_enable=memory` to `/boot/cmdline.txt`
1. Setup ssh keys on all pi:s
1. Configure the `hosts`-file with the correct hostnames
1. Configure version to use in `setup.yml`
1. Run `ansible-playbook -i hosts -K setup.yml` first time (will setup passwordless sudo)
1. Compile custom linux kernel with `kubernetes/raspberry-kernel/build.sh`. This is required for iscsi-storage.
1. Copy the kernel compiled `linux` dir to the `pi-controller`
1. Install the custom kernel with the following script on the pi-controller:
```sh
#!/bin/bash -eu

cd linux

sudo make ARCH=arm CROSS_COMPILE=arm-linux-gnueabihf- INSTALL_MOD_PATH=/ modules_install
sudo cp arch/arm/boot/zImage /boot/kernel-custom.img
sudo cp arch/arm/boot/dts/*.dtb /boot/
sudo cp arch/arm/boot/dts/overlays/*.dtb* /boot/overlays/
sudo cp arch/arm/boot/dts/overlays/README /boot/overlays/
```
1. Run `ansible-playbook -i hosts storage.yml` to setup storage
1. Kube config is written to `k3s_config.yml`

## Upgrade Kubernetes version
Update `k3s_version` in `setup.yml` and reapply
the ansible config: `ansible-playbook -i hosts setup.yml`

## Upgrade OS
You can upgrade Raspbian packages on all nodes with `ansible-playbook -i hosts upgrade.yml`.
If there is a linux kernel update, this will however overwrite the
self-compiled kernel on the pi-controller and things might stop working.
Then you will need to copy and reinstall the compiled kernel again.
