extern crate actix_web;
extern crate askama;
extern crate env_logger;
extern crate hostname;
extern crate mime_guess;

#[macro_use]
extern crate log;
#[macro_use]
extern crate rust_embed;

use actix_web::middleware::Logger;
use actix_web::{http, server, App, Body, HttpMessage, HttpRequest, HttpResponse};
use askama::Template;
use hostname::get_hostname;
use mime_guess::guess_mime_type;
use std::borrow::Cow;
use std::env;
use std::fmt;
use std::sync::{Arc, Mutex};

#[derive(RustEmbed)]
#[folder = "static/"]
struct Asset;

#[derive(Template)]
#[template(path = "index.html")]
struct Index<'a> {
    hostname: &'a str,
    health: &'a str,
}

enum AppHealth {
    Ok,
    Failed,
}

impl fmt::Display for AppHealth {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        match self {
            &AppHealth::Ok => write!(f, "healthy"),
            &AppHealth::Failed => write!(f, "failed"),
        }
    }
}

struct AppState {
    healthy: AppHealth,
}

fn index(req: &HttpRequest<Arc<Mutex<AppState>>>) -> HttpResponse {
    let hostname = get_hostname().unwrap();

    if let Some(hdr) = req.headers().get(http::header::ACCEPT) {
        if let Ok(s) = hdr.to_str() {
            if s.split(",").collect::<Vec<&str>>().contains(&"text/html") {
                return HttpResponse::Ok().content_type("text/html").body(
                    Index {
                        hostname: &hostname,
                        health: &req.state().lock().unwrap().healthy.to_string(),
                    }
                    .render()
                    .unwrap(),
                );
            }
        }
    }
    HttpResponse::Ok()
        .content_type("text/plain")
        .body(format!("Hello from {}\n", &hostname))
}

fn health(req: &HttpRequest<Arc<Mutex<AppState>>>) -> HttpResponse {
    let health = &req.state().lock().unwrap().healthy;
    match health {
        AppHealth::Ok => HttpResponse::Ok().body(health.to_string()),
        AppHealth::Failed => HttpResponse::InternalServerError().body(health.to_string()),
    }
}

fn fail(req: &HttpRequest<Arc<Mutex<AppState>>>) -> HttpResponse {
    req.state().lock().unwrap().healthy = AppHealth::Failed;

    HttpResponse::Ok().body(format!(
        "Server [{}] health set to failed\n",
        get_hostname().unwrap()
    ))
}

fn files(req: &HttpRequest<Arc<Mutex<AppState>>>) -> HttpResponse {
    let path = &req.path()["/static/".len()..];
    match Asset::get(path) {
        Some(content) => {
            let body: Body = match content {
                Cow::Borrowed(bytes) => bytes.into(),
                Cow::Owned(bytes) => bytes.into(),
            };
            HttpResponse::Ok()
                .content_type(guess_mime_type(path).as_ref())
                .body(body)
        }
        None => HttpResponse::NotFound().body("404 Not Found"),
    }
}

fn main() {
    env_logger::init();

    let port = match env::var("ECHO_SERVER_BIND_PORT") {
        Ok(p) => p,
        Err(_) => String::from("8080"),
    };

    info!("Config: PORT={}", port);

    let state = Arc::new(Mutex::new(AppState {
        healthy: AppHealth::Ok,
    }));

    server::new(move || {
        App::with_state(state.clone())
            .resource("/", |r| r.f(index))
            .resource("/health", |r| r.f(health))
            .resource("/fail", |r| r.method(http::Method::POST).f(fail))
            .resource("/static/{_:.*}", |r| r.method(http::Method::GET).f(files))
            .middleware(Logger::default())
            .finish()
    })
    .bind(format!("0.0.0.0:{}", port))
    .unwrap()
    .run();
}
